#include "ApolloUserInfoProducer.hpp"

ApolloUserInfoProducer::ApolloUserInfoProducer(std::string dir)
{ 
    DIR *dp;
    struct dirent *dirp;

    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Folder " << dir << " does not exist" << endl;
        return;
    }

    while ((dirp = readdir(dp)) != NULL) {
        string currFolderName = string(dirp->d_name);
        if (currFolderName.compare(".") == 0 || currFolderName.compare("..") == 0 || dirp->d_type != DT_DIR) {
            continue;
        }

        string userFileName = string(dir+"/"+currFolderName+"/user.json");

        ifstream userFile;
        userFile.open(userFileName);  //open the input file

        stringstream strStream;
        strStream << userFile.rdbuf();  //read the file
        string userInfo = strStream.str();  //str holds the content of the file

        addUserInfo(currFolderName, userInfo);  //you can do anything with the string!!!

        userFile.close();
    }
    closedir(dp);
}

void ApolloUserInfoProducer::addUserInfo(string username, string userInfo)
{
    this->userInfoMap.insert(pair<string, string>(username, userInfo));
}

void ApolloUserInfoProducer::run()
{
    m_face.setInterestFilter("/ndn/edu/umich/Apollo/user/",
                             bind(&ApolloUserInfoProducer::onInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&ApolloUserInfoProducer::onRegisterFailed, this, _1, _2));
    m_face.processEvents();
}

void ApolloUserInfoProducer::onInterest(const InterestFilter& filter, const Interest& interest)
{
  std::cout << "<< I: " << interest << std::endl;

  Name dataName(interest.getName());  

  unsigned int segmentNum = stoi(interest.getName().get(-1).toUri()); //segmentNum starts from 1  
  string username = dataName.get(-2).toUri();

  map<string, string>::iterator it;
  it = this->userInfoMap.find(username);
  string userInfo = it->second;
  std::string content;

  unsigned int maxSegmentNum = userInfo.size() / MAX_PACKET_SIZE + 1;

  // Create Data packet
  shared_ptr<Data> data = make_shared<Data>();
  data->setName(dataName);
  data->setFreshnessPeriod(time::seconds(10));

  if (segmentNum > maxSegmentNum) {
    cout << "Segment Number is too big" << endl;
    return;
  } else if (segmentNum == maxSegmentNum) {
    data->setContentType(9);
    content = userInfo.substr(MAX_PACKET_SIZE*(segmentNum-1));
  } else {
    content = userInfo.substr(MAX_PACKET_SIZE*(segmentNum-1), MAX_PACKET_SIZE);
  }
  
  data->setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());

  // Sign Data packet with default identity
  m_keyChain.sign(*data);
  // m_keyChain.sign(data, <identityName>);
  // m_keyChain.sign(data, <certificate>);

  // Return Data packet to the requester
  std::cout << ">> D: " << *data << std::endl;
  m_face.put(*data);
}


void ApolloUserInfoProducer::onRegisterFailed(const Name& prefix, const std::string& reason)
{
  std::cerr << "ERROR: Failed to register prefix \""
            << prefix << "\" in local hub's daemon (" << reason << ")"
            << std::endl;
  m_face.shutdown();
}

int
main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    string folderName = argv[1];

    ApolloUserInfoProducer *currProducer = new ApolloUserInfoProducer(folderName);
    currProducer->run();

    return 0;
}