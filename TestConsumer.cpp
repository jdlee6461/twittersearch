#include <ndn-cxx/face.hpp>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

using namespace ndn;
using namespace std;

class Consumer : noncopyable
{

public:
  void
  run(std::string testName)
  {
    Interest interest(Name(testName.c_str()));
    interest.setInterestLifetime(time::milliseconds(1000));
    interest.setMustBeFresh(true);

    m_face.expressInterest(interest,
                           bind(&Consumer::onData, this,  _1, _2),
                           bind(&Consumer::onTimeout, this, _1));

    std::cout << "Sending " << interest << std::endl;

    // processEvents will block until the requested data received or timeout occurs
    m_face.processEvents();
  }

private:
  void
  onData(const Interest& interest, const Data& data)
  {
    std::cout << data << std::endl;

    // ofstream newFile ("receiver2", ofstream::binary | ofstream::app);

    // uint8_t * memblock = (uint8_t*) data.getContent().value();
    // newFile.write((char *)memblock, data.getContent().value_size());

    // newFile.close();
    std::string receivedContent = reinterpret_cast<const char*>(data.getContent().value());
    receivedContent = receivedContent.substr(0, data.getContent().value_size());
    std::cout << "received content: " << receivedContent << std::endl;    
  }

  void
  onTimeout(const Interest& interest)
  {
    std::cout << "Timeout " << interest << std::endl;
  }

private:
  Face m_face;
};

int
main(int argc, char** argv)
{
  (void)argc;
  (void)argv;

  Consumer consumer;
  try {
    consumer.run(std::string(argv[1]));
  }
  catch (const std::exception& e) {
    std::cerr << "ERROR: " << e.what() << std::endl;
  }
  return 0;
}
