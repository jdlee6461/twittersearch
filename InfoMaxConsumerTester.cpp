#include <iostream>
#include <fstream>
#include <string>

#include "InfoMaxConsumer.hpp"

using namespace std;
using namespace ndn;
using namespace ndn::infomax;

int mode=0;

int
main(int argc, char** argv) {
	(void)argc;
	(void)argv;

    string root = argv[1];
    unsigned int count = stoi(argv[2]);

	Name nRoot = Name(root);
    InfoMaxConsumer *consumer = new InfoMaxConsumer (nRoot);
    
    for (unsigned int i=0; i<count; i++)
    {
        consumer->get();        
    }
    
	return 0;
}