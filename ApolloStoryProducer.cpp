#include "ApolloStoryProducer.hpp"

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;
using namespace std;

ApolloStoryProducer :: ApolloStoryProducer (string dir)
{

	ifstream rFile(dir+string("/task.json"));
	candidate = true;

	if (!rFile)
	{
		// cout << "task_info.json does not exist." << endl;
		candidate = false;
		return;
	}
	
	string line;
	getline (rFile, line);	

 	Reader reader;
    Value metaData;
    reader.parse(line, metaData);
    Value query = metaData["query"];
    Value analysisType = query["analysis_types"];
    string userName = metaData["created_by"].asString();

	Name nRoot = Name("ndn/edu/umich/Apollo");
    nRoot.append(Name(userName));
	nRoot.append(Name(query["dataset"].asString()));
	
	this->storyName = query["dataset"].asString();
	this->producer = new InfoMaxProducer(nRoot);

    vector<Name> nameList;
	vector<Data> dataList;

    string newestDir = getDir(dir);

    if (readFileintoNameAndData(newestDir, &nameList, &dataList, 10))  
    {
    	for(unsigned int i=0; i<nameList.size(); i++) {	
			producer->add(nameList[i], dataList[i]);
		}
	}	    

    if (!candidate)
    {
    	candidate = false; // unnecessary
    	return;
    }   

    cout << "Story < " << this->storyName << " > is fetched." << endl;
    cout << "Prefix: " << nRoot << endl;
}

void
ApolloStoryProducer :: run()
{
	producer->run();
}

Name
ApolloStoryProducer :: convertStringToName(string str)
{
	Name name = Name();
	for (unsigned int i=0; i<str.size(); i++)
		name.append(string(1,str[i]));
	return name;
}

bool
ApolloStoryProducer :: readFileintoNameAndData(string folderName, vector<Name> *nameList, vector<Data> *dataList, unsigned long long int freshnessPeriod)
{
	string line;	
	string name, content;
    string fileName = folderName+"/60m.ranked_model.txt.cleaned.sorted.with_image";
	ifstream readFile (fileName);
	
	if (readFile.is_open())
	{
		while (getline (readFile, line))
		{	
            unsigned int pos = line.find(" ");
            line = line.substr(0, pos);
			name = convertStringToName(line).toUri();			
            nameList->push_back(name);

            getline (readFile, line);
           	content = line;    
       	    Data data = Data(name);
           	data.setFreshnessPeriod(time::seconds(freshnessPeriod));
           	data.setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());
			dataList->push_back(data);					            
		}		
	} else
    {        
        cout << "Unable to open the ranked model file.\n";
        return false;
    }
    readFile.close();

	return true;
}

string
ApolloStoryProducer :: getDir (string dir)
{
    string maxTimeStampFolderName, currFolderName;
    int maxTimeStamp=0, currTimeStamp=0;
    DIR *dp;
    struct dirent *dirp;

    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Folder " << dir << " does not exist" << endl;
        return "";
    }

    while ((dirp = readdir(dp)) != NULL) {
        currFolderName = string(dirp->d_name);
        if (currFolderName.compare(".") == 0 || currFolderName.compare("..") == 0 || dirp->d_type != DT_DIR) {
            continue;
        }

        cout << currFolderName << endl;

        try {
            currTimeStamp = stoi(currFolderName);               
        } catch (exception) {
            cout << "Folder " << dir << " is not timestamp formatted" << endl;
            continue;
        }
        
        
        if (currTimeStamp > maxTimeStamp)
        {
            maxTimeStamp = currTimeStamp;
            maxTimeStampFolderName = dir + "/" + currFolderName;                            
        }
    }
    closedir(dp);

    cout << "max timestamp: " << maxTimeStampFolderName << endl;
    return maxTimeStampFolderName;    
}

string 
ApolloStoryProducer :: getStoryName()
{
	return this->storyName;
}

bool
ApolloStoryProducer :: getCandidateFlag()
{
	return this->candidate;
}

int
main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    string folderName = argv[1];

    ApolloStoryProducer *currProducer = new ApolloStoryProducer(folderName);

    if (!currProducer->getCandidateFlag()) {
        cout << "This folder is not candidate" << endl;
        return -1;
    }   

    currProducer->run();
    return 0;
}