#ifndef APOLLO_STORY_RECEIVER_HPP
#define APOLLO_STORY_RECEIVER_HPP

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <unistd.h>
#include <boost/algorithm/string.hpp>

#define KEYWORD_DIVIDER "%3A"

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;
using namespace std;
// Additional nested namespace could be used to prevent/limit name contentions
class ApolloStoryReceiver
{
public:
  ApolloStoryReceiver();

  void run();

private:
  void onInterest(const InterestFilter& filter, const Interest& interest);

  void onRegisterFailed(const Name& prefix, const std::string& reason);

private:
  Face m_face;
  KeyChain m_keyChain;
  string interestPrefix = "/ndn/edu/umich/Apollo/create/";
};

#endif