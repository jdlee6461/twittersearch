#!/usr/bin/python

import time
import os
import sys
import logging
import glob
import subprocess

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

#-----------------------------------------------------------------
# default variables
srcPath = sys.argv[1]
#-----------------------------------------------------------------
def main():

	srcPathLen = len(srcPath.split('/'))
	subprocess.Popen(["../ApolloStoryReceiver"])

	subprocess.Popen(["python", "migration.py", "/home/apollo/Apollo/now/bin/tasks", srcPath]).wait()
	subprocess.Popen(["python", "downloadImageFiles.py", srcPath]).wait()	

	while True:
		subprocess.Popen(["pkill", "-f", "ApolloStoryProducer"]).wait()
		subprocess.Popen(["pkill", "-f", "ApolloUserInfoProducer"]).wait()
		subprocess.Popen(["../ApolloUserInfoProducer",srcPath])	
		for root, dirs, files in os.walk(srcPath):
		    if (srcPath in root and len(root.split('/')) == srcPathLen + 2):
		    	subprocess.Popen(["../ApolloStoryProducer",root])

		subprocess.Popen(["python", "migration.py", "../apollo2", srcPath]).wait()
		subprocess.Popen(["python", "downloadImageFiles.py", srcPath]).wait()
		time.sleep(3600)		
	    	
if __name__ == '__main__':
    main()   
