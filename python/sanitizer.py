import sys
import logging
import json
import simplejson

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
#-----------------------------------------------------------------
def findMetaFile(inputFile):
	folderList = inputFile.split('/')
	del folderList[-1]
	del folderList[-1] 
	metaFile = '/'.join(folderList) + '/task.json'
	return metaFile
#-----------------------------------------------------------------
def main():

	inputFile = sys.argv[1]
	metaFile = findMetaFile(inputFile)

	source = open(inputFile, 'r')
	nameLines = open( inputFile, "r" ).readlines()[::2]
	dataLines = open( inputFile, "r" ).readlines()[1::2]
	output = open(inputFile+'.cleaned', 'w')

	with open(metaFile) as taskInfo:
		taskInfoJson = json.load(taskInfo)

	keywords = []

	if (taskInfoJson['query']['keyword1'] != ""): keywords.append(str(taskInfoJson['query']['keyword1']).lower())
	if (taskInfoJson['query']['keyword2'] != ""): keywords.append(str(taskInfoJson['query']['keyword2']).lower())
	if (taskInfoJson['query']['keyword3'] != ""): keywords.append(str(taskInfoJson['query']['keyword3']).lower())

	for i in range(len(dataLines))[::-1]:
		tweet_data = simplejson.loads(dataLines[i])
		tweet_text = tweet_data['text'].lower()
		sanityCheck = False
		for keyword in keywords:	
			if keyword in tweet_text:
				sanityCheck = True
				break

		if (sanityCheck == False):
			del nameLines[i]
			del dataLines[i]

	for i in range(0, len(nameLines)):
		print >> output, nameLines[i].replace('\n', '')
		print >> output, dataLines[i].replace('\n', '')

if __name__ == '__main__':
    main()
