import glob
import sys
import os
import shutil

path = sys.argv[1]
move_path = path + "/moved/"
min_timestamp = int(sys.argv[2])
#1429574400
files = glob.glob(path + "/*")

if not files:
    print >> sys.stderr, "No files present"
    sys.exit(1)

if not os.path.exists(move_path):
    os.makedirs(move_path)

for afile in files:
    last_slash_after = afile.rfind('/') + 1
    last_slash_after_dot = afile.find('.', last_slash_after)
    try:
        timestamp = int(afile[last_slash_after:last_slash_after_dot])
    except:
        print >> sys.stderr, "Skipping", afile
        continue
    if timestamp <= min_timestamp:
        print >> sys.stderr, "Moving", afile
        shutil.move(afile, move_path)
    else:
        print >> sys.stderr, "Skipping", afile

