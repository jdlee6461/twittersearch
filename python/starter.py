import os
import time
import subprocess
import sys

srcPath = sys.argv[1]

def child():
   #print 'A new child ',  os.getpid( )
   subprocess.Popen(["python", "cleanUp.py"]).wait()
   subprocess.Popen(["../ApolloUserInfoProducer", srcPath])
   subprocess.Popen(["../ApolloStoryReceiver"])
   subprocess.Popen(["python", "storyProducer.py", srcPath])

   subprocess.Popen(["python", "migration.py", "/home/apollo/Apollo/now/bin/tasks", srcPath]).wait()
   subprocess.Popen(["python", "downloadImageFiles.py", srcPath]).wait()
   os._exit(2)

def parent():
   while True:
      newpid = os.fork()
      if newpid == 0:
         child()
      else:
         pids = (os.getpid(), newpid)
         #print "parent: %d, child: %d" % pids
      time.sleep(3600)
#      if raw_input( ) == 'q': break

parent()
