import sys
import logging
import simplejson
import re
from collections import Counter
import json
#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
threshold = 0.75
userInfoFileName = str('user.json')
#-----------------------------------------------------------------
def stripSpecialCharacter(str1):
	newStr = re.sub("[^\w ]|-|'", "", str1)
	return newStr

def stripURL(str1):
	newStr = re.sub(r"http\S+"," ", str1)
	return newStr

def stripShortWords(str1):
	newStr = re.sub(r'\W*\b\w{1,4}\b', " ", str1)
	return newStr

def strip(str1):
	str1 = str1.lower()
	newStr1 = stripShortWords(stripSpecialCharacter(stripURL(str1)))
	newStr1 = ' '.join(newStr1.split())
	return newStr1

def jaccard(str1, str2):
	newStr1 = strip(str1)
	newStr2 = strip(str2)	
	set1 = set(newStr1.split(" "))
	set2 = set(newStr2.split(" "))

	return 1 - (float)(len(set1.intersection(set2))) / (float)(len(set1.union(set2)))

def checkSimilarTweets(index, dataList, windowSize):

	startIndex = 0
	endIndex =0
	newList = []
	targetText = dataList[index]['text']

	if (index <= windowSize/2):
		startIndex = 0
		endIndex = windowSize
	elif (index + windowSize/2 >= len(dataList)):
		startIndex = len(dataList) - windowSize
		endIndex = len(dataList) - 1 
	else:
		startIndex = index-windowSize/2
		endIndex = index+windowSize/2

	for i in range(startIndex, endIndex):
		if i != index:
			newList.append(dataList[i])

	for element in newList:
		if (jaccard (targetText, element['text']) <= threshold):
			# print "A: "+targetText.encode('utf-8')
			# print "B: "+element['text'].encode('utf-8')
			return 1

	return 0

def isSimilar (list1, str1):
	if len(list1) == 0:
		return False

	for word in list1:
		if word in str1 or str1 in word:
			return True

	return False

def isKeyword (str1):
	trivialWordSet = ["against", "better", "about", "going", "would", "could", "after", "about", "right", "their"]
	if str1 not in trivialWordSet:
		return True

	return False
#-----------------------------------------------------------------


def main():

	inputFile = sys.argv[1]
	nameLines = open( inputFile, "r" ).readlines()[::2]
	dataLines = open( inputFile, "r" ).readlines()[1::2]

	outputFile = open(inputFile, 'w')
	dataJson = []

	for i in range(0, len(dataLines)):
		dataJson.append(simplejson.loads(dataLines[i]))

	for i in range(0, len(dataJson)):
		dataJson[i]['haveSimilarTweets'] = checkSimilarTweets(i, dataJson, 10)

	wordList = []
	for i in range(0, len(dataJson)):
		newText = strip(dataJson[i]['text'])
		wordList = wordList + list(newText.split(" "))

	# for i in range(0, len(dataLines)):
	# 	temp = strip(dataJson[i]['text'])
	# 	wordList = wordList + list(temp.split(" "))
	# 	try:
	# 		if(dataJson[i]['haveSimilarTweets'] == 1):
	# 			continue
	# 	except KeyError:
	# 		dataJson[i]['haveSimilarTweets'] = 0
	# 		for j in range(i+1, len(dataLines)):
	# 			str1 = dataJson[i]['text']
	# 			str2 = dataJson[j]['text']
	# 			if jaccard(str1, str2) <= threshold:
	# 				dataJson[i]['haveSimilarTweets'] = 1
	# 				dataJson[j]['haveSimilarTweets'] = 1
	# 				break

	for i in range(0, len(dataLines)):
		print >> outputFile, nameLines[i].replace('\n', '')
		print >> outputFile, simplejson.dumps(dataJson[i]).replace('\n', '')

	mostCommonCounter = Counter(wordList).most_common()
	mostCommon = []
	count = 0
	while len(mostCommon) < 5:
		candidate = mostCommonCounter[count][0]
		count = count+1
		if not isKeyword(candidate):
			continue

		if not isSimilar(mostCommon, candidate):
			mostCommon.append(candidate)

	# Add frequent keywords to user info file
	pathComponent = inputFile.split('/')
	taskName = pathComponent[-3]
	pathComponent.pop()
	pathComponent.pop()
	pathComponent.pop()
	userInfoFilePath = '/'.join(pathComponent)+'/'+userInfoFileName

	with open(userInfoFilePath) as userInfo:
		userInfoJson = json.load(userInfo)

	for i in range(0, len(userInfoJson['tasks'])):
		if userInfoJson['tasks'][i]['query']['dataset'] == taskName:
			userInfoJson['tasks'][i]['query']['mostCommon'] = mostCommon

	newUserFile = open(userInfoFilePath, 'w')
	print >> newUserFile, json.dumps(userInfoJson)

if __name__ == '__main__':
    main()
