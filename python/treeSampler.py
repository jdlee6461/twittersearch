import sys
import logging
import simplejson

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
#-----------------------------------------------------------------
clusterSizeDB = dict () # [id_str : clustersize]
#-----------------------------------------------------------------
class Node(object):
    def __init__(self, name, data):
        self.name = name
        self.data = data
        self.children = []
        self.revisionCnt = 0
        self.childrenCnt = 0        
        self.weight = 0

    def addChild(self, obj):
        self.children.append(obj)
        self.childrenCnt += 1

    def visit(self):
    	self.revisionCnt += 1
#-----------------------------------------------------------------
def stringToName(nameLine):
	count = 0
	name = nameLine.split(' ')[0]
	nameList = []

	for character in name:
		nameList.append(character + str(count))		
	return nameList
#-----------------------------------------------------------------
def stringToWeight(nameLine):
	weight = nameLine.split(' ')[1]
	weight = str(round(float(weight), 5))
	return weight.replace('\n', '')
#-----------------------------------------------------------------
def getIDStr(dataLine):
	tweet_data = simplejson.loads(dataLine)
	id_str = tweet_data['id_str']
	return str(id_str)
#-----------------------------------------------------------------
def getClusterSize(nameLine):
	clusterSize = nameLine.split(' ')[1]
	clustersize = str(round(float(clusterSize), 5))
	return clusterSize.replace('\n', '')
#-----------------------------------------------------------------
def traverse(node):

	node.visit()
	if (node.childrenCnt == 0):			
		return node

	minRevisitCnt = sys.maxint
	minIndex = -1

	for i in range(0, len(node.children)):				
		child = node.children[i]
		if child.childrenCnt != 0 and child.revisionCnt == child.childrenCnt:
			continue
		if child.revisionCnt < minRevisitCnt:
			minRevisitCnt = child.revisionCnt
			minIndex = i

	return traverse(node.children[minIndex])
#-----------------------------------------------------------------
class Tree(object):
	def __init__(self):		
		self.nodeList = []
		self.nameList = set()
		self.root = Node("/O0", "")
		self.nameList.add(self.root.name)
		self.nodeList.append(self.root)
		self.leafCnt = 0
		
		self.keywordList = []

	def getNode(self, name):

		for i in range(0, len(self.nodeList)):
			if (name == self.nodeList[i].name):				
				return self.nodeList[i]
		return None

	def addNode(self, name, data):

		nameList = stringToName(name)
		nameString = '/'.join(nameList)
		nameString = '/'+nameString
		nodeWeight = stringToWeight(name)

		currentName = ""
		parentName = ""
		self.leafCnt += 1

		tweet_data = simplejson.loads(data)
		tweet_data['EMScore'] = nodeWeight

		try:
			tweet_data['clusterSize'] = clusterSizeDB[str(tweet_data['id_str'])]
		except KeyError:
			tweet_data['clusterSize'] = "1.0"

		data = simplejson.dumps(tweet_data)

		for nameComponent in nameList:
			parentName = currentName			
			currentName += "/"+nameComponent				

			if currentName not in self.nameList:
				self.nameList.add(currentName)
				if currentName == nameString:
					currentNode = Node(currentName, data)
				else:
					currentNode = Node(currentName, "")
				self.nodeList.append(currentNode)
			else:
				currentNode = self.getNode(currentName)				

			parentNode = self.getNode(parentName)
			if parentNode in self.nodeList:
				parentNode.addChild(currentNode)

	def sample(self):
		sampleNode = traverse(self.root)
		if sampleNode:
			return sampleNode

	def printNode(self):
		for node in self.nodeList:
			print node.name
			print node.revisionCnt
			print node.childrenCnt
#-----------------------------------------------------------------
def main():
	t = Tree()	

	referFile = sys.argv[2]
	nameLines2 = open( referFile, "r" ).readlines()[::2]
	dataLines2 = open( referFile, "r" ).readlines()[1::2]

	# Make reference for clusterzie 
	for i in range(0, len(nameLines2)):
		id_str = getIDStr(dataLines2[i])
		clusterSize = getClusterSize(nameLines2[i])
		clusterSizeDB[id_str] = clusterSize

	inputFile = sys.argv[1]
	nameLines = open( inputFile, "r" ).readlines()[::2]
	dataLines = open( inputFile, "r" ).readlines()[1::2]
	sortedOutput = open(inputFile+'.sorted', 'w')

	for i in range(0, len(nameLines)):
		t.addNode(nameLines[i], dataLines[i])
    
    # change model_get_image_link.py accordingly
	for i in range(0, 30):
		sampleNode = t.sample()
		print >> sortedOutput, sampleNode.name
		print >> sortedOutput, sampleNode.data.replace("\n","")

	weightedOutput = open( inputFile, "w" )
	for i in t.nodeList:
		if i.data:
			print >> weightedOutput, i.name
			print >> weightedOutput, i.data.replace("\n","")

if __name__ == '__main__':
    main()
