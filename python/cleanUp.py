#!/usr/bin/python

import os
import sys
import logging

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

#-----------------------------------------------------------------
# default variables
#-----------------------------------------------------------------
def main():
	os.system("pkill -f ApolloStoryProducer")
        os.system("pkill -f ApolloUserInfoProducer")
        os.system("pkill -f ApolloStoryReceiver")
	os.system("pkill -f downloadImageFiles.py")
	os.system("pkill -f model_get_image_link.py")

	logging.debug("pkill -f ApolloStoryProducer")

if __name__ == '__main__':
    main()   
