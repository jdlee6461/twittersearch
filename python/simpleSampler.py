import sys
import logging
import simplejson
import json

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
#-----------------------------------------------------------------
clusterSizeDB = dict () # [id_str : clustersize]
#-----------------------------------------------------------------
def stringToName(nameLine):
	count = 0
	name = nameLine.split(' ')[0]
	nameList = []

	for character in name:
		nameList.append(character + str(count))		
	return nameList
#-----------------------------------------------------------------
def stringToWeight(nameLine):
	weight = nameLine.split(' ')[1]
        weight = weight.replace('score:','')
	weight = str(round(float(weight), 5))
	return weight.replace('\n', '')
#-----------------------------------------------------------------
def getIDStr(dataLine):
	tweet_data = simplejson.loads(dataLine)
	id_str = tweet_data['id_str']
	return str(id_str)
#-----------------------------------------------------------------
def getClusterSize(nameLine):
	clusterSize = nameLine.split(' ')[1]
        clusterSize = clusterSize.replace('score:','')
	clustersize = str(round(float(clusterSize), 5))
	return clusterSize.replace('\n', '')
#-----------------------------------------------------------------
def addWeight(name, data):

	nameList = stringToName(name)
	nameString = '/'.join(nameList)
	nameString = '/'+nameString
	nodeWeight = stringToWeight(name)

	tweet_data = simplejson.loads(data)
	tweet_data['EMScore'] = nodeWeight

	try:
		tweet_data['clusterSize'] = clusterSizeDB[str(tweet_data['id_str'])]
	except KeyError:
		tweet_data['clusterSize'] = "1.0"

	data = simplejson.dumps(tweet_data)
	return data
#-----------------------------------------------------------------
def sort(nameLines, dataLines, sortedOutput):

	dataLinesJsons = []

	for dataLine in dataLines:
		dataLinesJsons.append(json.loads(dataLine))

	sortedTuples = sorted (zip(dataLinesJsons, nameLines), key=lambda x: float(x[0]['EMScore']), reverse=True)

	# sortedTuples = sorted(zip(DataLinesJsons, nameLines), key=lambda x: float(x[0]['EMScore']))

	dataLines, nameLines = zip(*sortedTuples)

	nameLines = list(nameLines)
	dataLines = list(dataLines)

	for i in range(0, len(nameLines)):
		print >> sortedOutput, nameLines[i].replace('\n', '')
	 	print >> sortedOutput, json.dumps(dataLines[i]).replace('\n', '')
#-----------------------------------------------------------------
def main():

	referFile = sys.argv[2]
	nameLines2 = open( referFile, "r" ).readlines()[::2]
	dataLines2 = open( referFile, "r" ).readlines()[1::2]

	# Make reference for clusterzie 
	for i in range(0, len(nameLines2)):
		id_str = getIDStr(dataLines2[i])
		clusterSize = getClusterSize(nameLines2[i])
		clusterSizeDB[id_str] = clusterSize

	inputFile = sys.argv[1]
	nameLines = open( inputFile, "r" ).readlines()[::2]
	dataLines = open( inputFile, "r" ).readlines()[1::2]
	sortedOutput = open(inputFile+'.sorted', 'w')

	
	for i in range(0, len(nameLines)):
		dataLines[i] = addWeight(nameLines[i], dataLines[i])

	sort(nameLines, dataLines, sortedOutput)
    
    # change model_get_image_link.py accordingly
	# for i in range(0, 30):
	# 	sampleNode = t.sample()
	# 	print >> sortedOutput, sampleNode.name
	# 	print >> sortedOutput, sampleNode.data.replace("\n","")

	# weightedOutput = open( inputFile, "w" )
	# for i in t.nodeList:
	# 	if i.data:
	# 		print >> weightedOutput, i.name
	# 		print >> weightedOutput, i.data.replace("\n","")

if __name__ == '__main__':
    main()
