#!/usr/bin/python

import time
import os
import sys
import logging
import glob
import subprocess

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

#-----------------------------------------------------------------
# default variables
srcPath = sys.argv[1]
#-----------------------------------------------------------------
def main():

	srcPathLen = len(srcPath.split('/'))

        for root, dirs, files in os.walk(srcPath):
                if (srcPath in root and len(root.split('/')) == srcPathLen + 2):
                        subprocess.Popen(["../ApolloStoryProducer", root])

if __name__ == '__main__':
    main()   
