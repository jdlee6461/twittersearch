#!/usr/bin/python

import os
import sys
import glob
import logging
import subprocess

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

#-----------------------------------------------------------------
# default variables
srcPath = sys.argv[1]
rankedModelFileName = str('60m.ranked_model.txt')
rankedModelCleanFileName = rankedModelFileName+".cleaned"
clusterSizeDBFileName = str('clusterSize.txt')
sortedFileName = rankedModelCleanFileName+".sorted"
withImageFileName = sortedFileName+".with_image"
#-----------------------------------------------------------------

def main():
	# traverse root directory, and list directories as dirs and files as files
	srcPathLen = len(srcPath.split('/'))

	for root, dirs, files in os.walk(srcPath):
	    path = root.split('/')

	    if (len(path) == srcPathLen + 2):
	    	logging.warning("Check whether this folder is dataset: "+os.path.basename(root))
	    	folders = os.walk(root).next()[1]	
	    	logging.debug("folders: "+str(folders))

	    	if folders:
		    	sortedFolders = sorted(folders)    	
		    	newestFolder = sortedFolders[-1]
		    	if not newestFolder.isdigit():
		    		logging.CRITICAL("wrong folder. Check downloadImageFiles.py")

		    	newestFolderPath = root+"/"+sortedFolders[-1]    
		    	newestRankedModelFilePath = newestFolderPath+"/"+rankedModelFileName
		    	newestRankedModelCleanFilePath = newestFolderPath+"/"+rankedModelCleanFileName		    	
		    	newestSortedFilePath = newestFolderPath+"/"+sortedFileName
		    	newestWithImageFilePath = newestFolderPath+"/"+withImageFileName
		    	clusterSizeDBFilePath = newestFolderPath + "/" + clusterSizeDBFileName

		    	logging.debug("newestFolderPath: "+newestFolderPath)

		    	# if glob.glob(newestWithImageFilePath):
       #                          logging.debug("there is already "+ newestWithImageFilePath)
       #                          continue
       #                  else:
                logging.debug("Execute: "+newestRankedModelFilePath)
                os.system("python sanitizer.py "+newestRankedModelFilePath)
                os.system("python checkSimilarTweets.py "+newestRankedModelCleanFilePath)
                os.system("python simpleSampler.py "+newestRankedModelCleanFilePath+" "+clusterSizeDBFilePath)
                # os.system("python treeSampler.py "+newestRankedModelCleanFilePath+" "+clusterSizeDBFilePath)
                # os.system("python model_get_image_link.py "+newestSortedFilePath)
                subprocess.Popen(["python", "model_get_image_link.py", newestSortedFilePath])
	    	
if __name__ == '__main__':
    main()
