import sys
from get_image_link import get_image_link
import simplejson
import glob
import re
import logging
import urllib
#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)
#------------------------------------------------------------------------------
sortList = [] # if it is in top 30 tweets
#------------------------------------------------------------------------------
def get_image_link_from_tweet_text(text):
    # return get_image_link({'text': text})
    return get_image_link({'text': text})
#------------------------------------------------------------------------------
def is_image_page(url):
    return True
    #print 'url:', url
    #for s in IMG_KEYWORDS:
    #    if url.find(s) >= 0:
    #        return True
    #return False
#------------------------------------------------------------------------------
def get_claim_with_image(claim, folder):

    image_link_res = get_image_link_from_tweet_text(claim['text'])
    if image_link_res != None and is_image_page(image_link_res[0]):
        image_link = image_link_res[1]
        print >> sys.stderr, 'Found One:', repr(image_link_res[0]), repr(image_link)
    else:
        image_link = ''    

    claim['claim_img'] = ''

    if (image_link != ''):
        urllib.urlretrieve(image_link, folder+"/"+claim['id_str'])
        claim['claim_img'] = image_link

    return claim
#------------------------------------------------------------------------------
def get_claim_without_image(claim):

    image_link = ''
    claim['claim_img'] = image_link

    return claim
#-----------------------------------------------------------------
def getIDStr(dataLine):
    tweet_data = simplejson.loads(dataLine)
    id_str = tweet_data['id_str']
    return str(id_str)
#------------------------------------------------------------------------------
def main():
    # open new output file
    f = sys.argv[1]
    input_file = open(f, 'r')        
    output_file = open(f + '.with_image', 'w')

    folderList = f.split('/')
    folderList.pop()
    folder = '/'.join(folderList)
    folder += '/img'

    count = 0
    limit = 30 # the number images to be downloaded
    while 1:
        meta_line = input_file.readline()
        if not meta_line:
            break
        data_line = input_file.readline()
        print >> output_file, meta_line,
        # process dataline
        tweet_data = simplejson.loads(data_line)
        
        if count < limit:
            get_claim_with_image(tweet_data, folder)
        else:
            get_claim_without_image(tweet_data)

        count = count + 1
        print >> output_file, simplejson.dumps(tweet_data)        

    input_file.close()
    output_file.close()

if __name__ == '__main__':
    main()
