#!/usr/bin/python

import os
import glob
import sys
import json
import logging
from shutil import copyfile

#-----------------------------------------------------------------
# logging.basicConfig(format='%(asctime)s %(message)s', level=logging.CRITICAL)
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

#-----------------------------------------------------------------
# default variables
srcPath = sys.argv[1]
dstPath = sys.argv[2]
taskInfoOldFileName = str('task_info.json')
taskInfoNewFileName = str('task.json')
userInfoFileName = str('user.json')
rankedModelFileName = str('60m.ranked_model.txt')
diversityModelFileName = str('clusterSize.txt')
users = dict()
#-----------------------------------------------------------------
class User(object):
    def __init__(self, name):
        self.username = name
        self.tasks = []

    def addTask(self, task):
    	self.tasks.append(task)
#-----------------------------------------------------------------
def readTaskInfo (taskInfoFileFullPath):

	with open(taskInfoFileFullPath) as taskInfo:
		taskInfoJson = json.load(taskInfo)

	try:
		userName = taskInfoJson['created_by']
		isActive = taskInfoJson['status']
	except KeyError:
		logging.debug("No username!")
		return None

	if(isActive != "active"):
		logging.debug("Not active status")
		return None

	userNameStr = str(userName)
	taskInfoStr = json.dumps(taskInfoJson)

	try:
		users[userNameStr].addTask(taskInfoJson)
	except KeyError:
		users[userNameStr] = User(userNameStr)
		users[userNameStr].addTask(taskInfoJson)

	analysisTypes = taskInfoJson['query']['analysis_types']
	datasetName = taskInfoJson['query']['dataset']

	if str('DiversitySocial') not in analysisTypes:
		logging.debug("This task has no DiversitySocial")
		return None

	if str('Diversity') not in analysisTypes:
		logging.debug("This task has no Diversity")
		return None

	userDirectory = dstPath+'/'+userName
	if not os.path.exists(userDirectory):
		logging.debug("mkdir "+userDirectory)
		os.makedirs(userDirectory)	

	dataDirectory = userDirectory+'/'+datasetName	
	if not os.path.exists(dataDirectory):
		logging.debug("mkdir "+dataDirectory)
		os.makedirs(dataDirectory)

	logging.debug("data directory: "+dataDirectory)

	copyfile(taskInfoFileFullPath, dataDirectory+"/"+taskInfoNewFileName)
	return dataDirectory
#-----------------------------------------------------------------
def findTheNewestFile(root, dataDirectory):

	files = glob.glob(root+'/*.60m.ranked_model.txt')	    	
	if files:
		sortedFiles = sorted(files)    	
		newestFullPath = sortedFiles[-1]

	temp = root.split('/')
	temp[-1] = "Diversity"
	diversityFolder = '/'.join(temp)

	newestFileName = newestFullPath.split('/')[-1]
	newestTimestamp = newestFileName.split('.')[0]
	timeStampDirectory = dataDirectory+"/"+newestTimestamp
	imgDirectory = timeStampDirectory+"/img"

	diversityFileName = diversityFolder + "/" + newestFileName

	if not os.path.exists(timeStampDirectory):
		logging.debug("mkdir "+timeStampDirectory)
		os.makedirs(timeStampDirectory)	

	if not os.path.exists(imgDirectory):
		logging.debug("mkdir "+imgDirectory)
		os.makedirs(imgDirectory)	
        
        diversityFile = glob.glob(diversityFileName)
        diversitySocialFile = glob.glob(newestFullPath)

        if diversityFile and diversitySocialFile:
            copyfile(newestFullPath, timeStampDirectory+"/"+rankedModelFileName)
            copyfile(diversityFileName, timeStampDirectory+"/"+diversityModelFileName)
            return True
        else:
            return False
        
#-----------------------------------------------------------------

def main():
	# traverse root directory, and list directories as dirs and files as files

	for root, dirs, files in os.walk(srcPath):
	    path = root.split('/')
	    if str('.git') in path:
	    	continue	  

	    if taskInfoOldFileName in files:	    	
	    	taskInfoFileFullPath = '/'.join(path)+'/'+taskInfoOldFileName
	    	dataDirectory = readTaskInfo(taskInfoFileFullPath)
	    	if dataDirectory == None:
	    		continue

	    if (os.path.basename(root) == "DiversitySocial" and dataDirectory):
	    	findTheNewestFile(root, dataDirectory)
                continue
	
	dstPathLen = len(dstPath.split('/'))
	for root, dirs, files in os.walk(dstPath):
	    path = root.split('/')

	    if (len(path) == dstPathLen + 1):
	    	username = os.path.basename(root)
	    	f = file(dstPath+'/'+username+'/'+userInfoFileName, 'w')
	    	userInfo = json.dumps(users[username].__dict__)
	    	f.write(userInfo)
	    	f.close()

if __name__ == '__main__':
    main()   
