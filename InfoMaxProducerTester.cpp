#include <iostream>
#include <fstream>
#include <string>
#include "InfoMaxProducer.hpp"
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#include <json/json.h>

using namespace std;
using namespace ndn;
using namespace ndn::infomax;
using namespace Json;

bool
readFileintoString(string fileName, string *content)
{
	string line;
	ifstream readFile (fileName);
	
	if (readFile.is_open())
	{
		while (getline (readFile, line))
		{
			*content += line;
		}
		readFile.close();
	}

	return true;
}

bool
setContentFromFile(Data *data, string fileName, unsigned long long int freshnessPeriod)
{
	data->setFreshnessPeriod(time::seconds(freshnessPeriod));
	string content;	

	readFileintoString(fileName, &content);
	data->setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());
	return true;
}

Name
convertStringToName(string str)
{
	Name name = Name();
	for (unsigned int i=0; i<str.size(); i++)
		name.append(string(1,str[i]));
	return name;
}

bool
readFileintoNameAndData(string fileName, vector<Name> *nameList, vector<Data> *dataList, unsigned long long int freshnessPeriod)
{
	cout << "works?" << endl;
	string line;	
	string name, content;	
	ifstream readFile (fileName);
	
	if (readFile.is_open())
	{
		while (getline (readFile, line))
		{								
			unsigned int pos = line.find(" ");
			line = line.substr(0,pos);
			name = convertStringToName(line).toUri();									
            nameList->push_back(name);

            getline (readFile, line);
           	content = line;    
       	    Data data = Data(name);
           	data.setFreshnessPeriod(time::seconds(freshnessPeriod));
           	data.setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());
			dataList->push_back(data);					            
		}		
	}else
    {
        cout << "Unable to open the file.\n";
    }
    readFile.close();

	return true;
}

string
getdir (string dir)
{

	string maxTimeStampFileName, currFileName;
	int maxTimeStamp=0, currTimeStamp=0;
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return "getdir() Error";
    }

    while ((dirp = readdir(dp)) != NULL) {    	
    	currFileName = string(dirp->d_name);
    	if(currFileName.find("60m.model.txt") != string::npos)
    	{    		
    		currTimeStamp = atoi(currFileName.substr(0, 10).c_str());    		
    		
    		if (currTimeStamp > maxTimeStamp)
    		{
    			maxTimeStamp = currTimeStamp;
    			maxTimeStampFileName = dir + "/" + currFileName;        	    			
    		}
    	}
    }
    closedir(dp);

    cout << maxTimeStampFileName << endl;
    return maxTimeStampFileName;
}

bool
readDirintoNameAndData(string dir, vector<Name> *nameList, vector<Data> *dataList)
{
	string file = getdir(dir);
    readFileintoNameAndData(file, nameList, dataList, 10); 
    return 0;
}

int
main(int argc, char** argv) {
	(void)argc;
	(void)argv;

	bool candidate = false;
	ifstream rFile (argv[1]+string("/task_info.json"));
	string line;
	getline (rFile, line);

 	Reader reader;
    Value metaData;
    reader.parse(line, metaData);
    Value query = metaData["query"];
    Value analysisType = query["analysis_types"];

    for (unsigned int i=0; i<analysisType.size(); i++)
    {
    	if (analysisType[i].asString() == string("Diversity"))
    	{
    		candidate = true;    	
    		break;
    	}
    }

    if (!candidate)
    {
    	cout << "This folder doesn't have Diversity folder." << endl;
    	return -1;
    }

    cout << "dataset: " << query["dataset"].asString() << endl;;   

	Name nRoot = Name("Apollo");
	nRoot.append(Name(query["dataset"].asString()));
	
	InfoMaxProducer *producer = new InfoMaxProducer(nRoot);	

	vector<Name> nameList;
	vector<Data> dataList;

	string dir = string(argv[1]+string("/analysis/Diversity"));	
    readDirintoNameAndData(dir, &nameList, &dataList);   
    
	for(unsigned int i=0; i<nameList.size(); i++) {	
		producer->add(nameList[i], dataList[i]);
	}

	// Name del = Name("a");
	// producer->deletes(del);
	// producer->prioritize();    	
	producer->run();    	
    
	return 0;
}