CC = g++
INC = -I. -Ilibs
FLAGS = -g -W -Wall -std=c++11
LIBS = -lboost_system -lboost_thread

all: ApolloStoryProducer InfoMaxConsumerTester ApolloUserInfoProducer TestConsumer ApolloStoryReceiver

InfoMaxConsumerTester: TreeNode.o InfoMaxConsumer.o InfoMaxConsumerTester.cpp
	$(CC) $(FLAGS) $(INC) $^ -o $@ `pkg-config --cflags --libs libndn-cxx`

ApolloStoryProducer: Prioritizers.o TreeNode.o InfoMaxProducer.o ApolloStoryProducer.cpp ApolloStoryProducer.hpp
	$(CC) $(FLAGS) $(INC) $^ -o $@ `pkg-config --cflags --libs libndn-cxx jsoncpp`

ApolloUserInfoProducer: ApolloUserInfoProducer.cpp ApolloUserInfoProducer.hpp
	$(CC) $(FLAGS) $(INC) $^ -o $@ `pkg-config --cflags --libs libndn-cxx`

ApolloStoryReceiver: ApolloStoryReceiver.cpp ApolloStoryReceiver.hpp
	$(CC) $(FLAGS) $(INC) $^ -o $@ `pkg-config --cflags --libs libndn-cxx`

TestConsumer: TestConsumer.cpp
	$(CC) $(FLAGS) $(INC) $^ -o $@ `pkg-config --cflags --libs libndn-cxx`

InfoMaxConsumer.o: InfoMaxConsumer.cpp InfoMaxConsumer.hpp InvalidPrefixException.hpp
	$(CC) -c $(FLAGS) $(INC) $< -o $@

InfoMaxProducer.o: InfoMaxProducer.cpp InfoMaxProducer.hpp InvalidPrefixException.hpp
	$(CC) -c $(FLAGS) $(INC) $< -o $@ `pkg-config --cflags --libs jsoncpp`	

TreeNode.o: TreeNode.cpp TreeNode.hpp
	$(CC) -c $(FLAGS) $(INC) $< -o $@

Prioritizers.o: Prioritizers.cpp Prioritizers.hpp
	$(CC) -c $(FLAGS) $(INC) $< -o $@ `pkg-config --cflags --libs jsoncpp`	

clean:
	$(RM) -r *.o ApolloStoryProducer InfoMaxConsumerTester ApolloUserInfoProducer TestConsumer ApolloStoryReceiver