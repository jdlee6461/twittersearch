#ifndef INVALID_PREFIX_EXCEPTION_HPP
#define INVALID_PREFIX_EXCEPTION_HPP 

#include <iostream>
#include <exception>

using namespace std;
// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;

/**
 * Exception defination for invalid prefix.
 */
class InvalidPrefixException: public exception
{
	virtual const char* what() const throw()
  	{
    	return "Invalid prefix.";
  	}
};
static InvalidPrefixException prefixException;

#endif