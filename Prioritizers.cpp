#include "Prioritizers.hpp"
#include <climits>
// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;
// Additional nested namespace could be used to prevent/limit name contentions

vector<TreeNode *>* simplePrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector);
vector<TreeNode *>* complexPrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector);
vector<TreeNode *>* sortPrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector);
vector<TreeNode *>* votingPrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector);

vector<TreeNode *>* dummy(TreeNode *root);
TreeNode* getNextPriorityNode(TreeNode *root);
TreeNode* getNextPriorityNodeWithPrefixMatch(TreeNode *root, vector<TreeNode*>* prioritizedVector);
int getMaxNumSharedPrefix(TreeNode *root, vector<TreeNode*>*prioritizedVector);
int getMinNumSharedPrefix(TreeNode *root, vector<TreeNode*>*prioritizedVector);
vector<TreeNode *>* mostSharedPrefixFirst(TreeNode *currentNode, vector<TreeNode*>* prioritizedVector);
int binarySearch(vector<TreeNode*>* prioritizedVector, Name &name);
string filterString(string input);

string dataToString(Data *data) {
	string content = reinterpret_cast<const char*>(data->getContent().value());
	content = content.substr(0, data->getContent().value_size());

	Json::Reader reader;
    Json::Value metaData;
    reader.parse(content, metaData);
    content = metaData["text"].asString();
    return content;
}

string filterString(string input) {

	input = boost::algorithm::to_lower_copy(input);

	string format1 = " ";
	boost::regex regURL(R"(http\S+)"); // Filter
	string output1 = boost::regex_replace (input, regURL, format1);    

	string format2 = "";
	boost::regex regSpecialCharacter(R"([^\w ]|-|')"); // Filter
	string output2 = boost::regex_replace (output1, regSpecialCharacter, format2);    

	boost::regex regShortWords(R"(\W*\b\w{1,4}\b)"); // Filter
	string output3 = boost::regex_replace (output2, regShortWords, format1);    

	// Please optimize this part later. Remove empty set element.
	vector<string> temp;
	boost::split(temp, output3, boost::is_any_of(" "));
	for (int i=temp.size()-1; i>=0; i--){
		if (temp.at(i) == "")
			temp.erase(temp.begin() + i);
	}

	string finalOutput = boost::join(temp, " ");

	return finalOutput;
}

double jaccard(string A, string B){
	
    
    // boost::regex regSpecialCharacter(" (http://)([^ ]*)| (https://)([^ ]*)| is |Is | are |Are | the |The | a |A | an |An |\\.|,|@|:|;|#|!|\\?| &amp;|'s|'t|'"); // Filter
    string newStringA = filterString(A);
    string newStringB = filterString(B);

    std::set<string> setA;
    std::set<string> setB;
    std::set<string> setIntersect;
    std::set<string> setUnion;

    boost::split(setA, newStringA, boost::is_any_of(" "), boost::algorithm::token_compress_on);
    boost::split(setB, newStringB, boost::is_any_of(" "), boost::algorithm::token_compress_on);

    set_intersection(setA.begin(), setA.end(), setB.begin(), setB.end(), inserter(setIntersect, setIntersect.begin()));
    set_union(setA.begin(), setA.end(), setB.begin(), setB.end(), inserter(setUnion, setUnion.begin()));

    for (set<string>::iterator it = setA.begin(); it != setA.end(); it++) {
   		cout << "\"" << *it << "\"" << endl;
	}

	cout << endl;

	for (set<string>::iterator it = setB.begin(); it != setB.end(); it++) {
   		cout << "\"" << *it << "\"" << endl;
	}

    double result = 1 - double(setIntersect.size()) / double(setUnion.size());

    cout << "Jaccard: " << result << endl;

    return result;
}

vector<TreeNode *>* prioritizeWithAlgorithm(int algoNum, TreeNode *root, vector<TreeNode*>* prioritizedVector) {
	
	switch (algoNum) {
		case 0:
			// cout << "simple algorithm" << endl;
			return simplePrioritizer(root, prioritizedVector);
			break;
		case 1:
			// cout << "complex algorithm" << endl;
			return complexPrioritizer(root, prioritizedVector);
			break;
		case 2:            
            return mostSharedPrefixFirst(root, prioritizedVector);
            break;
        case 3:
        	return sortPrioritizer(root, prioritizedVector);
        	break;		
        case 4:
        	return votingPrioritizer(root, prioritizedVector);
        	break;		
		default:
			return dummy(root);
	}
}

bool treeNodePointerComparator(TreeNode* i, TreeNode* j) { 
	return (i->getName().toUri())<(j->getName().toUri()); 	
}

bool treeNodeWeightComparator(TreeNode* i, TreeNode* j) { 
	return (i->getWeight())>(j->getWeight()); 
}

bool treeNodeClusterSizeComparator(TreeNode* i, TreeNode* j) { 
	return (i->getClusterSize())>(j->getClusterSize()); 
}

vector<TreeNode *>* mostSharedPrefixFirst(TreeNode *currentNode, vector<TreeNode*>* prioritizedVector) {
    
    vector<TreeNode*> *nearestNeighborVector = new vector<TreeNode*>(*prioritizedVector); 
    std::sort((*nearestNeighborVector).begin(), (*nearestNeighborVector).end(), treeNodePointerComparator);

    return nearestNeighborVector;
}



vector<TreeNode *>* simplePrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector) {
	(void)root;
	
	unsigned int numOfLeafNodes = prioritizedVector->size();	
	prioritizedVector->clear();

	while(root->getRevisionCount() < numOfLeafNodes) {		
		prioritizedVector->push_back(getNextPriorityNode(root));
	}

	return prioritizedVector;
}

vector<TreeNode *>* complexPrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector) {
	(void)root;
	
	vector<TreeNode*> *sentList = new vector<TreeNode*>;
	TreeNode* sentNode;
	// prioritizedVector->clear();
	sentNode = prioritizedVector->at(0);
	sentNode->markNode(true);
	sentList->push_back(sentNode);
	
	while(sentList->size() < prioritizedVector->size())
	{	
		int leastSharedPrefixCountNow = INT_MAX;
		for (unsigned int i=1; i<prioritizedVector->size(); i++)
		{
			if (!prioritizedVector->at(i)->isNodeMarked())
			{
				if (leastSharedPrefixCountNow > getMaxNumSharedPrefix(prioritizedVector->at(i), sentList))
				{
					leastSharedPrefixCountNow = getMaxNumSharedPrefix(prioritizedVector->at(i), sentList);
					sentNode = prioritizedVector->at(i);
				}
			}			
		}

		// cout << sentNode->getName() << " is selected " << leastSharedPrefixCountNow << endl;

		sentNode->markNode(true);
		sentList->push_back(sentNode);
	}

	// prioritizedVector->clear();
	// prioritizedVector = sentList;
	// return prioritizedVector;
	return sentList;
}

vector<TreeNode *>* sortPrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector) {
	(void) root;

	vector<TreeNode*> *sortedVector = new vector<TreeNode*>(*prioritizedVector); 
    std::sort((*sortedVector).begin(), (*sortedVector).end(), treeNodeWeightComparator);

    return sortedVector;
}

vector<TreeNode *>* votingPrioritizer(TreeNode *root, vector<TreeNode*>* prioritizedVector) {
	(void) root;

	vector<TreeNode*> *votingVector = new vector<TreeNode*>(*prioritizedVector); 
    std::sort((*votingVector).begin(), (*votingVector).end(), treeNodeClusterSizeComparator);

    return votingVector;
}

vector<TreeNode *>* dummy(TreeNode *root) {
	vector<TreeNode *> *prioritizedVector = new vector<TreeNode *> ();
	prioritizedVector->push_back(root);

	// unordered_map<string, TreeNode*> child = root->getChildrenMap();
	// for(auto it = child.begin(); it != child.end(); ++it ) {
	// 	TreeNode *n = it->second;
	// 	prioritizedVector->push_back(n);
	// }

	vector<TreeNode*> children = root->getChildren();
	for(unsigned int i=0; i<children.size(); i++ ) {
		TreeNode *n = children[i];
		prioritizedVector->push_back(n);
	}

	return prioritizedVector;
}

int matchingNamesComponent(Name name1, Name name2) {
	int minUriCount = std::min(name1.size(), name2.size());
	int matchCount = 0;
	for(int i = 0; i < minUriCount; i++) {
		if(name1.get(i) == name2.get(i)) {
			matchCount ++;
		} else {
			return matchCount;
		}
	}
	return matchCount;
}


int binarySearch(vector<TreeNode*>* prioritizedVector, Name &name) {
	int left = 0;
	int right = (int)(prioritizedVector->size())-1;
    int mid = (right+left)/2;

	while(left <= right)
    {
        mid = (right+left)/2;
        //iterator it = prioritizedVector[mid]; 
        //TreeNode * currentNode = *it;
        TreeNode * currentNode = (*prioritizedVector)[mid]; 
        
        Name currentNodeName = currentNode->getName();
        
        if (currentNodeName.toUri() == name.toUri()) { 
            return mid;
        }
        else if (name.toUri() > currentNodeName.toUri()) {
            left = mid + 1; 
        }
        else if (name.toUri() < currentNodeName.toUri()) {
            right = mid - 1; 
        }
    }
    return -1;
}

vector<TreeNode *>* mostSharedPrefixNodes(vector<TreeNode*>* prioritizedVector, Name &name, int count) {
	vector<TreeNode *>* mostSharedNodes = new std::vector<TreeNode *> ();
	int namePosition = binarySearch(prioritizedVector, name);
	if(namePosition == -1) {
		cout << "cannot find targetNode" << endl;
		return NULL;
	}

	string mainContent = dataToString(prioritizedVector->at(namePosition)->getData());

	int startIndex = 0;
	int endIndex = 0;
	double threshold = 0.80;

	if (namePosition <= count / 2) {
		startIndex = 0;
		endIndex = count;
	} else if (namePosition + count / 2 >= prioritizedVector->size()) {
		startIndex = prioritizedVector->size() - count;
		endIndex = prioritizedVector->size() - 1;
	} else {
		startIndex = namePosition - count / 2;
		endIndex = namePosition + count / 2;
	}

	for (int i=startIndex; i<=endIndex; i++) {
		if (i != namePosition) {
			string matchingContent = dataToString(prioritizedVector->at(i)->getData());	
			if (jaccard (mainContent, matchingContent) <= threshold) {
				mostSharedNodes->push_back(prioritizedVector->at(i));
			}
		}
	}
	
	// int prioritizedVectorSize = prioritizedVector->size();

	// int low = namePosition - 1;
	// int high = namePosition + 1;

	// int lowPrefixMatchScore = -1;
	// int highPrefixMatchScore = -1;
	// int i=0;
	// double threshold = 0.80;

	// while(i < count) {
	// 	if(low < 0 && high >= prioritizedVectorSize) {
	// 		return mostSharedNodes;
	// 	}
	// 	else if(low < 0) {
	// 		if(prioritizedVector->at(high)->getName().size() < 25) {  // This is because of iOS NDN library bug
	// 			// cout << "adding this since it is shorter than 25 : " << prioritizedVector->at(high)->getName() << prioritizedVector->at(high)->getName().size() << endl;				
	// 			string matchingContent = dataToString(prioritizedVector->at(high)->getData());				
	// 			cout << "Data: " << matchingContent << endl;

	// 			if (jaccard (mainContent, matchingContent) < threshold) {
	// 				mostSharedNodes->push_back((*prioritizedVector)[high]);
	// 			}				
	// 			i++;
	// 		}
	// 		high ++;
	// 	}
	// 	else if (high >= prioritizedVectorSize) {
	// 		if(prioritizedVector->at(low)->getName().size() < 25) {  // This is because of iOS NDN library bug
	// 			// cout << "adding this since it is shorter than 25 : " << prioritizedVector->at(low)->getName() << prioritizedVector->at(low)->getName().size() << endl;
	// 			string matchingContent = dataToString(prioritizedVector->at(low)->getData());				
	// 			cout << "Data: " << matchingContent << endl;

	// 			if (jaccard (mainContent, matchingContent) < threshold) {
	// 				mostSharedNodes->push_back((*prioritizedVector)[low]);
	// 			}
	// 			i++;
	// 		}
	// 		low --;	
	// 	}
	// 	else {
	// 		if(lowPrefixMatchScore == -1) {
	// 			lowPrefixMatchScore = matchingNamesComponent(name, (*prioritizedVector)[low]->getName());
	// 		}
	// 		if(highPrefixMatchScore == -1) {
	// 			highPrefixMatchScore = matchingNamesComponent(name, (*prioritizedVector)[high]->getName());
	// 		}
	// 		if(highPrefixMatchScore > lowPrefixMatchScore) {
	// 			if(prioritizedVector->at(high)->getName().size() < 25) {  // This is because of iOS NDN library bug
	// 				// cout << "adding this since it is shorter than 25 : " << prioritizedVector->at(high)->getName() << prioritizedVector->at(high)->getName().size() << endl;
	// 				string matchingContent = dataToString(prioritizedVector->at(high)->getData());				
	// 				cout << "Data: " << matchingContent << endl;

	// 				if (jaccard (mainContent, matchingContent) < threshold) {
	// 					mostSharedNodes->push_back((*prioritizedVector)[high]);
	// 				}
	// 				i++;
	// 			}
	// 			high ++;
	// 			highPrefixMatchScore = -1;		
	// 		}
	// 		else {
	// 			if(prioritizedVector->at(low)->getName().size() < 25) {  // This is because of iOS NDN library bug
	// 				// cout << "adding this since it is shorter than 25 : " << prioritizedVector->at(low)->getName() << prioritizedVector->at(low)->getName().size() << endl;
	// 				string matchingContent = dataToString(prioritizedVector->at(low)->getData());				
	// 				cout << "Data: " << matchingContent << endl;

	// 				if (jaccard (mainContent, matchingContent) < threshold) {
	// 					mostSharedNodes->push_back((*prioritizedVector)[low]);
	// 				}
	// 				i++;
	// 			}
	// 			low --;
	// 			lowPrefixMatchScore = -1;		
	// 		}
	// 	}
	// }


	// for (int i = 0; i < count; i++) {
	// 	if(low < 0 && high >= prioritizedVectorSize) {
	// 		return mostSharedNodes;
	// 	}
	// 	else if(low < 0) {
	// 		mostSharedNodes->push_back((*prioritizedVector)[high]);
	// 		high ++;
	// 	}
	// 	else if (high >= prioritizedVectorSize) {
	// 		mostSharedNodes->push_back((*prioritizedVector)[low]);
	// 		low --;	
	// 	}
	// 	else {
	// 		if(lowPrefixMatchScore == -1) {
	// 			lowPrefixMatchScore = matchingNamesComponent(name, (*prioritizedVector)[low]->getName());
	// 		}
	// 		if(highPrefixMatchScore == -1) {
	// 			highPrefixMatchScore = matchingNamesComponent(name, (*prioritizedVector)[high]->getName());
	// 		}
	// 		if(highPrefixMatchScore > lowPrefixMatchScore) {
	// 			mostSharedNodes->push_back((*prioritizedVector)[high]);
	// 			high ++;
	// 			highPrefixMatchScore = -1;		
	// 		}
	// 		else {
	// 			mostSharedNodes->push_back((*prioritizedVector)[low]);
	// 			low --;
	// 			lowPrefixMatchScore = -1;		
	// 		}
	// 	}
	// }
	return mostSharedNodes;
}

TreeNode* getNextPriorityNode(TreeNode *root) {
	if(root == NULL) {
		return NULL;
	}		

	root->updateRevisionCount(root->getRevisionCount()+1);

	if(root->isDataNode() && !(root->isNodeMarked())) {				
		// cout << "upload:" << root->getName() << endl;
		root->markNode(true);		
		return root;
	}
	
	// unordered_map<string, TreeNode*> children = root->getChildrenMap();		
	vector<TreeNode*> children = root->getChildren();

	if(children.size() > 0) {		
		unsigned long long int leastRevisionCountNow = ULLONG_MAX;
		TreeNode *nodeWithLeastCount = NULL;
		
		for(unsigned int i=0; i<children.size(); i++ )
		{			
			TreeNode* child = children[i];
			if(child->getRevisionCount() < child->getTreeSize() || child->getRevisionCount() == 0) {
				if(nodeWithLeastCount == NULL || (nodeWithLeastCount != NULL && leastRevisionCountNow > child->getRevisionCount())) {
					nodeWithLeastCount = child;
					leastRevisionCountNow = child->getRevisionCount();
				}
			}
		}

		return getNextPriorityNode(nodeWithLeastCount);			
	}
	return NULL;
}

// TreeNode* getNextPriorityNodeWithPrefixMatch(TreeNode *root, vector<TreeNode*>* prioritizedVector) {

// 	vector<TreeNode*> candidateNode;

// 	if(root == NULL) {
// 		return NULL;
// 	}	

// 	root->updateRevisionCount(root->getRevisionCount()+1);

// 	if(root->isDataNode() && !(root->isNodeMarked())) {				
// 		cout << "upload:" << root->getName() << endl;
// 		root->markNode(true);		
// 		return root;
// 	}

// 	vector<TreeNode*> children = root->getChildren();

// 	if(children.size() > 0) {		
// 		unsigned long long int leastRevisionCountNow = ULLONG_MAX;
// 		int leastSharedPrefixCountNow = INT_MAX;
// 		TreeNode *nodeWithLeastCountLeastPrefixMatch = NULL;
		
// 		for(unsigned int i=0; i<children.size(); i++ )
// 		{			
// 			TreeNode* child = children[i];
// 			int numSharePrefix = getMaxNumSharedPrefix(child, prioritizedVector);

// 			cout << child->getName() << "'s numSharePrefix = ' " << numSharePrefix << " and leastSharedPrefixCountNow = " << leastSharedPrefixCountNow << endl;

// 			if (leastSharedPrefixCountNow > numSharePrefix)
// 			{
// 				cout << " leastSharedPrefixCountNow > numSharePrefix " << endl;
// 				leastSharedPrefixCountNow = numSharePrefix;
// 				candidateNode.clear();
// 				candidateNode.push_back(child);
// 			} else if (leastSharedPrefixCountNow == numSharePrefix)
// 			{
// 				cout << " leastSharedPrefixCountNow == numSharePrefix " << endl;
// 				candidateNode.push_back(child);	
// 			}
// 		}

// 		if (candidateNode.empty())
// 		{
// 			cout << "something wrong" << endl;
// 		} else if (candidateNode.size() == 1)
// 		{						
// 			nodeWithLeastCountLeastPrefixMatch = candidateNode[0];
// 		} else
// 		{			
// 			for(unsigned int i=0; i<candidateNode.size(); i++)
// 			{
// 				TreeNode* child = candidateNode[i];
// 				if(child->getRevisionCount() < child->getTreeSize() || child->getRevisionCount() == 0) {
// 					if(nodeWithLeastCountLeastPrefixMatch == NULL || (nodeWithLeastCountLeastPrefixMatch != NULL && leastRevisionCountNow > child->getRevisionCount())) {
// 						nodeWithLeastCountLeastPrefixMatch = child;
// 						leastRevisionCountNow = child->getRevisionCount();
// 					}
// 				}	
// 			}
			
// 		}
// 		cout << nodeWithLeastCountLeastPrefixMatch->getName() << " is selected to upload" << endl;	
// 		return getNextPriorityNodeWithPrefixMatch(nodeWithLeastCountLeastPrefixMatch, prioritizedVector);			
// 	}
// 	return NULL;
// }

int getMaxNumSharedPrefix(TreeNode *root, vector<TreeNode*>*prioritizedVector) {

	int max=0;
	// cout << "vector size: " << prioritizedVector->size() << endl;
	for (unsigned int i=0; i<prioritizedVector->size(); i++)
	{
		// cout << "max: " << max << "root->getNumSharedPrefix(prioritizedVector->at(i)) " << root->getNumSharedPrefix(prioritizedVector->at(i)) << endl;
		if(max < root->getNumSharedPrefix(prioritizedVector->at(i)))
		{
			max = root->getNumSharedPrefix(prioritizedVector->at(i));
		}
	}
	return max;
}

int getMinNumSharedPrefix(TreeNode *root, vector<TreeNode*>*prioritizedVector) {

	int min=INT_MAX;
	// cout << "vector size: " << prioritizedVector->size() << endl;
	for (unsigned int i=0; i<prioritizedVector->size(); i++)
	{
		// cout << "max: " << max << "root->getNumSharedPrefix(prioritizedVector->at(i)) " << root->getNumSharedPrefix(prioritizedVector->at(i)) << endl;
		if(min > root->getNumSharedPrefix(prioritizedVector->at(i)))
		{
			min = root->getNumSharedPrefix(prioritizedVector->at(i));
		}
	}
	return min;
}
