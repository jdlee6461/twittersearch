#include "ApolloStoryReceiver.hpp"

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;

ApolloStoryReceiver::ApolloStoryReceiver()
{

}

void ApolloStoryReceiver::run()
{
  m_face.setInterestFilter(this->interestPrefix,
                           bind(&ApolloStoryReceiver::onInterest, this, _1, _2),
                           RegisterPrefixSuccessCallback(),
                           bind(&ApolloStoryReceiver::onRegisterFailed, this, _1, _2));
  m_face.processEvents();
}

void ApolloStoryReceiver::onInterest(const InterestFilter& filter, const Interest& interest)
{
  std::cout << "<< I: " << interest << std::endl;

  // Create new name, based on Interest's name
  Name dataName(interest.getName());

  Name interestPrefixName = Name(interestPrefix);
  Name interestName = interest.getName();

  string userName = interestName.get(interestPrefixName.size()).toUri();
  string storyName = interestName.get(interestPrefixName.size()+1).toUri();
  string keywordsStr = interestName.get(interestPrefixName.size()+2).toUri();
  string locationStr = interestName.get(interestPrefixName.size()+3).toUri();

  vector<string> keywords = vector<string>();
  boost::split(keywords, keywordsStr, boost::is_any_of(KEYWORD_DIVIDER), boost::token_compress_on);
  vector<string> location = vector<string>();
  boost::split(location, locationStr, boost::is_any_of(KEYWORD_DIVIDER), boost::token_compress_on);


  std::cout << "<< username: " << userName << std::endl;     
  std::cout << "<< story: " << storyName << std::endl;     
  for (unsigned int i=0; i<keywords.size(); i++)
    std::cout << "<< keyword " << i+1 << ": " << keywords.at(i) << std::endl;   
  for (unsigned int i=0; i<location.size(); i++)
    std::cout << "<< location " << i+1 << ": " << location.at(i) << std::endl;   

  std::string command = "curl 'http://apollo2.cs.illinois.edu/now-bin/now.py/create_task?keyword1=" + keywords[0] + "&keyword2=" + keywords[1] + "&keyword3=" + keywords[0] + "&dataset=" + storyName + "&latitude=&longitude=&radius=&crawler=search_api&created_by=ndn&query_type=NOT&analysis_types=\%5B\%22Diversity\%22\%5D&default_analysis=Diversity&permission=private' -H 'Host: apollo2.cs.illinois.edu'";
  system(command.c_str());

  static const std::string content = "ACK";

  // Create Data packet
  shared_ptr<Data> data = make_shared<Data>();
  data->setName(dataName);
  data->setFreshnessPeriod(time::seconds(10));
  data->setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());

  // Sign Data packet with default identity
  m_keyChain.sign(*data);
  // m_keyChain.sign(data, <identityName>);
  // m_keyChain.sign(data, <certificate>);

  // Return Data packet to the requester
  std::cout << ">> D: " << *data << std::endl;
  m_face.put(*data);
}


void ApolloStoryReceiver::onRegisterFailed(const Name& prefix, const std::string& reason)
{
  std::cerr << "ERROR: Failed to register prefix \""
            << prefix << "\" in local hub's daemon (" << reason << ")"
            << std::endl;
  m_face.shutdown();
}


int
main(int argc, char** argv)
{
  (void)argc;
  (void)argv;

  ApolloStoryReceiver *producer = new ApolloStoryReceiver();
  producer->run();
  return 0;
}
