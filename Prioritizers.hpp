#ifndef PRIORITIZERS_HPP
#define PRIORITIZERS_HPP

#include "TreeNode.hpp"
#include <ndn-cxx/face.hpp>
#include <vector>
#include <set>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <json/json.h>

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;
// Additional nested namespace could be used to prevent/limit name contentions

vector<TreeNode *>* prioritizeWithAlgorithm(int algoNum, TreeNode *root, vector<TreeNode*>* prioritizedVector);

vector<TreeNode *>* mostSharedPrefixNodes(vector<TreeNode*>* prioritizedVector, Name &name, int count);

#endif