#ifndef APOLLO_USERINFO_PRODUCER_HPP
#define APOLLO_USERINFO_PRODUCER_HPP

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <errno.h>

#define MAX_PACKET_SIZE 1000

using namespace std;
// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
using namespace ndn;
// Additional nested namespace could be used to prevent/limit name contentions

class ApolloUserInfoProducer
{
public:
  ApolloUserInfoProducer(std::string dir);

  void run();

private:
  void onInterest(const InterestFilter& filter, const Interest& interest);
  void onRegisterFailed(const Name& prefix, const std::string& reason);
  void addUserInfo(string username, string userInfo);

private:
  Face m_face;
  KeyChain m_keyChain;
  map<string, string> userInfoMap;
};

#endif
